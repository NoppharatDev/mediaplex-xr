import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Headers,
} from '@nestjs/common';
import { SignupService } from './signup.service';
import { CreateSignupDto } from './dto/create-signup.dto';
import { ApiBody, ApiTags } from '@nestjs/swagger';

@ApiTags('Signup')
@Controller('signup')
export class SignupController {
  constructor(private readonly signupService: SignupService) {}

  @Post('/guest')
  signUpGuest(@Body() createSignupDto: CreateSignupDto) {
    return this.signupService.signUpGuest(createSignupDto);
  }
  @Post('/google')
  signUpGoogle(@Body() createSignupDto: CreateSignupDto, @Headers('MyToken') MyToken) {
    return this.signupService.signUpGoogle(createSignupDto, MyToken);
  }

  /*@Get()
  findAll() {
    return this.signupService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.signupService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateSignupDto: UpdateSignupDto) {
    return this.signupService.update(+id, updateSignupDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.signupService.remove(+id);
  }*/
}
