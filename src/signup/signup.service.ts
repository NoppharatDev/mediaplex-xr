import { Injectable } from '@nestjs/common';
import { CreateSignupDto } from './dto/create-signup.dto';
import { db, dbref } from 'src/firebase.config';
import { set, ref, get, child, push } from 'firebase/database';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class SignupService {
  constructor(private jwtService: JwtService) {}

  async checkFound(val, propName) {
    const getUserAll = await get(child(dbref, 'users')).then((snapshot) => {
      if (snapshot.exists()) {
        return snapshot.val();
      } else {
        console.log('No data available');
      }
    });

    let found = false;
    for (const property in getUserAll) {
      if (val == getUserAll[property][propName]) {
        found = true;
      }
    }

    return found;
  }

  async signUpGuest(createSignupDto: CreateSignupDto) {
    const { email, firstName, lastName, password } = createSignupDto;
    const hashedPassword = await bcrypt.hash(password, 12);
    const postRef = ref(db, 'users/');
    const newPostRef = push(postRef);
    const uid = newPostRef.key;
    const jwt = await this.jwtService.signAsync({
      uid: uid,
      email: email,
      isOnline: false,
    });

    const foundEmail = await this.checkFound(email, 'email');
    if (!foundEmail) {
      await set(ref(db, 'users/' + uid), {
        authType: 'guest',
        email: email,
        firstName: firstName,
        lastName: lastName,
        password: hashedPassword,
        token: jwt,
      });
      return {
        success: true,
        token: jwt,
        message: 'Singup with guest successfully',
      };
    } else {
      return {
        success: false,
        message: 'Singup with guest failed (Duplicate Email)',
      };
    }
  }

  async signUpGoogle(createSignupDto: CreateSignupDto, MyToken: any) {
    const { accountObj } = createSignupDto;
    const resData = accountObj._tokenResponse;
    const postRef = ref(db, 'users/');
    const newPostRef = push(postRef);
    const uid = newPostRef.key;
    const jwt = await this.jwtService.signAsync({
      uid: uid,
      guid: accountObj.user.uid,
      email: accountObj.user.email,
      isOnline: false,
    });

    const foundEmail = await this.checkFound(resData.email, 'email');
    if (!foundEmail) {
      await set(ref(db, 'users/' + uid), {
        authType: 'google',
        email: resData.email,
        firstName: resData.firstName,
        lastName: resData.lastName,
        photoUrl: resData.photoUrl,
        token: jwt,
      });
      return {
        success: true,
        token: jwt,
        message: 'Singup with google successfully',
      };
    } else {
      return {
        success: false,
        message: 'Singup with google failed (Duplicate Email)',
      };
    }
  }

  /*findAll() {
    return `This action returns all signup`;
  }

  findOne(id: number) {
    return `This action returns a #${id} signup`;
  }

  update(id: number, updateSignupDto: UpdateSignupDto) {
    return `This action updates a #${id} signup`;
  }

  remove(id: number) {
    return `This action removes a #${id} signup`;
  }*/
}
