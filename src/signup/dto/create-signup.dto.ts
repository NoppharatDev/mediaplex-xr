export class CreateSignupDto {
    uid: string;
    authType: string;
    email: string;
    firstName: string;
    lastName: string;
    password?: string;
    token?: string;
    accountObj?: any;
}