export class CreateSigninDto {
    email: string;
    password?: string;
    accountObj?: any;
}