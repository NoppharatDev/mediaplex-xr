import { Injectable } from '@nestjs/common';
import { db, dbref } from 'src/firebase.config';
import { update, set, ref, get, child, push } from 'firebase/database';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { CreateSigninDto } from './dto/create-signin.dto';
import { isJwtExpired } from 'jwt-check-expiration';

@Injectable()
export class SigninService {
  constructor(private jwtService: JwtService) {}

  async getUser(val, propName) {
    const getUserAll = await get(child(dbref, 'users')).then((snapshot) => {
      if (snapshot.exists()) {
        return snapshot.val();
      } else {
        console.log('No data available');
      }
    });

    let found = false,
      data = {},
      uid = null;
    for (const property in getUserAll) {
      if (val == getUserAll[property][propName]) {
        found = true;
        data = getUserAll[property];
        uid = property;
      }
    }

    return {
      found: found,
      data: data,
      uid: uid,
    };
  }

  async signInGuest(createSignupDto: CreateSigninDto) {
    const { email, password } = createSignupDto;
    const hashedPassword = await bcrypt.hash(password, 12);
    const raw = { email, hashedPassword };
    let success = false;
    let message = null;
    let token = null;

    const getUser = await this.getUser(email, 'email');
    const data = getUser.data;
    const newToken = await this.jwtService.signAsync({
      uid: getUser.uid,
      email: email,
      isOnline: true,
    });
    if (getUser.found) {
      if (data['authType'] == 'guest') {
        const passCompare = await bcrypt.compare(password, data['password']);
        if (passCompare) {
          if(isJwtExpired(data['token'])) {
            await update(ref(db, 'users/' + getUser.uid), {
              token: newToken,
            });
            success = true;
            token = newToken;
            message = 'Singin with guest successfully * Singin with guest (Token Expied) *';
          } else {
            const newData = await this.jwtService.verifyAsync(data['token']);
            if(newData.isOnline) {
              message ='Singin with guest failed (Found online for this email)';
            } else {
              await update(ref(db, 'users/' + getUser.uid), {
                token: newToken,
              });
              success = true;
              token = newToken;
              message = 'Singin with guest successfully';
            }
          }
        }
      } else {
        message ='Singin with guest failed ({authType: Google} auth type not matching)';
      }
    } else {
      message = 'Not found Email...';
    }

    return {
      success: success,
      message: message,
      raw: raw,
      token: token
    };
  }

  async signInGoogle(createSignupDto: CreateSigninDto) {
    const { accountObj } = createSignupDto;
    const email = accountObj.user.email;
    const uid = accountObj.user.uid;
    const getUser = await this.getUser(email, 'email');
    const data = getUser.data;
    const raw = accountObj;
    let success = false;
    let token = null;
    let message = null;
    const newToken = await this.jwtService.signAsync({
      uid: getUser.uid,
      guid: uid,
      email: email,
      isOnline: true,
    });

    if (getUser.found) {
      if (data['authType'] == 'google') {
        if(isJwtExpired(data['token'])) {
          await update(ref(db, 'users/' + getUser.uid), {
            token: newToken,
          });
          success = true;
          token = newToken;
          message = 'Singin with google successfully * Singin with google (Token Expied) *';
        } else {
          const newData = await this.jwtService.verifyAsync(data['token']);
          if(newData.isOnline) {
            message ='Singin with google failed (Found online for this email)';
          } else {
            await update(ref(db, 'users/' + getUser.uid), {
              token: newToken,
            });
            success = true;
            token = newToken;
            message = 'Singin with google successfully';
          }
        }
      } else {
        message = 'Singin with google failed ({authType: Guest} auth type not matching)';
      }
    } else {
      message = 'Not found Email...';
    }

    return {
      success: success,
      message: message,
      raw: raw,
      token: token
    };
  }
}
