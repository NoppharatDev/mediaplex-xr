import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { SigninService } from './signin.service';
import { CreateSigninDto } from './dto/create-signin.dto';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('Signin')
@Controller('signin')
export class SigninController {
  constructor(private readonly signinService: SigninService) {}

  @Post('guest')
  signInGuest(@Body() createSigninDto: CreateSigninDto) {
    return this.signinService.signInGuest(createSigninDto);
  }

  @Post('google')
  signInGoogle(@Body() createSigninDto: CreateSigninDto) {
    return this.signinService.signInGoogle(createSigninDto);
  }
}
