
export class CreateQueueDto {
    name : string;
    quantity: number;
    list: string;  
    email ?: string; 
    token : string; 
    User :string ; 
    uid :string ;
}

