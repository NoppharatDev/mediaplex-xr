import { Delete, Injectable } from '@nestjs/common';
import { CreateQueueDto } from './dto/create-queue.dto';
import { UpdateQueueDto } from './dto/update-queue.dto';
import { db, dbref } from 'src/firebase.config';
import { set, ref, get, child, push, update  ,remove} from 'firebase/database';
import { JwtService } from '@nestjs/jwt';
import e from 'express';

@Injectable()
export class QueueService {
  constructor(private JwtService: JwtService) {}
  async checkRooms(val, propName) {
    const getdata = await get(child(dbref, 'room')).then((snapshot) => {
      if (snapshot.exists()) {
        return snapshot.val();
      } else {
        return {
          success : false ,
          message: 'room not found',
        };
      }
    });
    let found = false,
      data = {},
      uid = null,
      quantity = null;
    for (const property in getdata) {
      if (val == getdata[property][propName]) {
        found = true;
        uid = property;
        quantity = getdata[property]['quantity'];
      }
    }
    return {
      found: found,
      uid: uid,
      quantity: quantity,
    };
  }

  async checklist(name) {
    const postRef = ref(db, 'room/');
    const foundDataName = await this.checkRooms(name, 'name');
    const getdata = await get(
      child(dbref, `room/${foundDataName.uid}/list`),
    ).then((snapshot) => {
      if (snapshot.exists()) {
        return snapshot.size;
      } else {
        return {
          val: 0,
        };
      }
    });

    return getdata;
  }

  async checkWintingRoom(name) {
    const postRef = ref(db, 'room/');
    const foundDataName = await this.checkRooms(name, 'name');
    const getdata = await get(
      child(dbref, `room/${foundDataName.uid}/withingroom`),
    ).then((snapshot) => {
      if (snapshot.exists()) {
        return snapshot.size;
      } else {
        return {
          val: 0,
        };
      }
    });
    return getdata;
  }
  async getUser(val, propName) {
    const getUserAll = await get(child(dbref, 'users')).then((snapshot) => {
      if (snapshot.exists()) {
        return snapshot.val();
      } else {
        return{
          success : false ,
          message: 'No data available',
        }
      }
    });
    let found = false,
      data = {},
      uid = null;
    for (const property in getUserAll) {
      if (val == getUserAll[property][propName]) {
        found = true;
        data = getUserAll[property];
        uid = property;
      }
    }
      return {
      found: found,
      data: data,
      uid: uid,
    };
  }

  async createRoom(createQueueDto: CreateQueueDto) {
    const { name } = createQueueDto;
    const postRef = ref(db, 'room/');
    const newPostRef = push(postRef);
    const uid = newPostRef.key;
    const foundRoom = await this.checkRooms(name, 'name');
    if (!foundRoom.found) {
      const data = await set(ref(db, 'room/' + uid), {
        name: createQueueDto.name,
        quantity: createQueueDto.quantity,
      });
      return {
        success : true ,
        message: 'finished room',
      };
    } else {
      return {
        success : false ,
        message: 'Failed to create a room. because there is already this room',
      };
    }
  }

  async enterInRoom(createQueueDto: CreateQueueDto) {
    const { token, name } = createQueueDto;
    const postRef = ref(db, 'room/');
    const foundDataUser = await this.getUser(token, 'token');
    const foundDataName = await this.checkRooms(name, 'name');
    const getlist = await this.checklist(name);
    const getWentingRoom = await this.checkWintingRoom(name);
    const quantityCheck = foundDataName.quantity;
    if (foundDataName.found) {
      if (getlist['val'] == 0) {
        const data = await set(
          ref(db, `room/${foundDataName.uid}/list/${foundDataUser.uid}`),
          {
            User: createQueueDto.token,
          },
        );
        return {
          success : true ,
          message: 'successfully entered the room',

        };
      }
      if (getlist < quantityCheck) {
        const data = await set(
          ref(db, `room/${foundDataName.uid}/list/${foundDataUser.uid}`),
          {
            User: createQueueDto.token,
          },
        );
        return {
          success : true ,
          message: 'finished room',
        };
      } else {
        const data = await set(
          ref(db, `room/${foundDataName.uid}/withingroom/${foundDataUser.uid}`),
          {
            User: createQueueDto.token,
          },
        );
        if (getWentingRoom['val'] == 0) {
          return {
            success : true ,
            message: `previous queue 0 people`,
          };
        }
        return {
          success : true ,
          message: `previous queue ${getWentingRoom} `,
        };
      }
    }
  }

  async getData() {
    const getUserAll = await get(child(dbref, 'room')).then((snapshot) => {
      if (snapshot.exists()) {
        return snapshot.val();
      } else {
       return {
          success : false ,
          message: 'No data available',
      }  
      }
    });
    return {
      getUserAll
    }
  }

  async logoutRoom (createQueueDto:CreateQueueDto){
    const {token ,name} = createQueueDto;
    const postRef = ref(db, 'room/');
    const foundDataUser = await this.getUser(token, 'token');
    const foundDataName = await this.checkRooms(name, 'name');
    if(foundDataName.found){
      const data = await remove(ref(db, `room/${foundDataName.uid}/list/${foundDataUser.uid}`));
      return {
        success : true ,
        message: 'exited the room successfully',
      };
    }
  }

  async logoutWintingRoom (createQueueDto:CreateQueueDto){
    const {token ,name} = createQueueDto;
    const postRef = ref(db, 'room/');
    const foundDataUser = await this.getUser(token, 'token');
    const foundDataName = await this.checkRooms(name, 'name');
    if(foundDataName.found){
      const data = await remove(ref(db, `room/${foundDataName.uid}/withingroom/${foundDataUser.uid}`));
      return {
        success : true ,
        message: 'Get out of the waiting room.',
      };
    }
  }

  findAll() {
    return `This action returns all queue`;
  }

  findOne(id: number) {
    return `This action returns a #${id} queue`;
  }

  update(id: number, updateQueueDto: UpdateQueueDto) {
    return `This action updates a #${id} queue`;
  }

  remove(id: number) {
    return `This action removes a #${id} queue`;
  }
}
