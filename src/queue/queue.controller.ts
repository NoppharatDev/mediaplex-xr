import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { QueueService } from './queue.service';
import { CreateQueueDto } from './dto/create-queue.dto';
import { UpdateQueueDto } from './dto/update-queue.dto';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('Room')
@Controller('queue')
export class QueueController {
  constructor(private readonly queueService: QueueService) {}

  @Post('/buildRoom')
  create(@Body() createQueueDto: CreateQueueDto) {
    return this.queueService.createRoom(createQueueDto);
  }

  @Post('/joinRoom')
  enterInRoom(@Body() createQueueDto: CreateQueueDto) {
    return this.queueService.enterInRoom(createQueueDto);
  }

  @Post('/checklist')
  checklist(@Body() createQueueDto: CreateQueueDto) {
    return this.queueService.checklist(createQueueDto);
  }
  @Post('/checkoutRoom')
  logoutRoom(@Body() createQueueDto: CreateQueueDto) {
    return this.queueService.logoutRoom(createQueueDto);
  }
  @Post('/checkoutWintingRoom')
  logoutWintingRoom(@Body() createQueueDto: CreateQueueDto) {
    return this.queueService.logoutWintingRoom(createQueueDto);
  }

  @Get('/getRoom')
  findAll() {
    return this.queueService.getData();
  }

  // @Get(':id')
  // findOne(@Param('id') id: string) {
  //   return this.queueService.findOne(+id);
  // }

  // @Patch(':id')
  // update(@Param('id') id: string, @Body() updateQueueDto: UpdateQueueDto) {
  //   return this.queueService.update(+id, updateQueueDto);
  // }

  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.queueService.remove(+id);
  // }
}
