import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { SigninModule } from './signin/signin.module';
import { SignupModule } from './signup/signup.module';
import { SignoutModule } from './signout/signout.module';
import { QueueModule } from './queue/queue.module';
@Module({
  imports: [UsersModule, SigninModule, SignupModule, SignoutModule, QueueModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
