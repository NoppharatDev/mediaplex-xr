import { Test, TestingModule } from '@nestjs/testing';
import { SignoutController } from './signout.controller';
import { SignoutService } from './signout.service';

describe('SignoutController', () => {
  let controller: SignoutController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SignoutController],
      providers: [SignoutService],
    }).compile();

    controller = module.get<SignoutController>(SignoutController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
