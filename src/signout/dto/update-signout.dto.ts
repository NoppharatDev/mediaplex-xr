import { PartialType } from '@nestjs/swagger';
import { CreateSignoutDto } from './create-signout.dto';

export class UpdateSignoutDto extends PartialType(CreateSignoutDto) {}
