import { Controller, Get, Post, Body, Patch, Param, Delete, Headers } from '@nestjs/common';
import { SignoutService } from './signout.service';
import { CreateSignoutDto } from './dto/create-signout.dto';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('Signout')
@Controller('signout')
export class SignoutController {
  constructor(private readonly signoutService: SignoutService) {}

  @Post()
  signOut(@Body() createSignoutDto: CreateSignoutDto, @Headers('MyToken') MyToken) {
    return this.signoutService.signOut(createSignoutDto, MyToken);
  }
}
