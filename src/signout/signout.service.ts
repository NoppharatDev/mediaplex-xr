import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { CreateSignoutDto } from './dto/create-signout.dto';
import { isJwtExpired } from 'jwt-check-expiration';
import { db, dbref } from 'src/firebase.config';
import { update, set, ref, get, child, push } from 'firebase/database';

@Injectable()
export class SignoutService {
  constructor(private jwtService: JwtService) {}

  async signOut(createSignoutDto: CreateSignoutDto, MyToken: any) {
    let success = false;
    let message = null;
    if (isJwtExpired(MyToken)) {
      success = true;
      message = 'Signout successfully. you token expired';
    } else {
      const jwtData = await this.jwtService.verifyAsync(MyToken);
      // console.log(jwtData.isOnline);      
      if (jwtData.isOnline) {
        const newToken = await this.jwtService.signAsync({
          uid: jwtData.uid,
          email: jwtData.email,
          isOnline: false,
        });
        await update(ref(db, 'users/' + jwtData.uid), {
          token: newToken,
        });
        success = true;
        message = `Signout successfully`;
      } else {
        message = `Signout failed (You is offline)`;
      }
    }
    return {
      success: success,
      message: message,
      raw: createSignoutDto,
      // token: MyToken,
    };
  }
}
