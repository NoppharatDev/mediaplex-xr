import { Module } from '@nestjs/common';
import { SignoutService } from './signout.service';
import { SignoutController } from './signout.controller';
import { JwtModule } from '@nestjs/jwt';

@Module({
  imports: [
    JwtModule.register({
      secret: 'secret',
      signOptions: { expiresIn: '1d' },
    }),
  ],
  controllers: [SignoutController],
  providers: [SignoutService]
})
export class SignoutModule {}
