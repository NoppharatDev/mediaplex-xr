import { Controller, Get, Post, Body, Patch, Param, Delete, Headers } from '@nestjs/common';
import { UsersService } from './users.service';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('Users')
@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Get('info')
  getUserInfo(@Headers('MyToken') MyToken) {
    return this.usersService.getUserInfo(MyToken);
  }

  @Get()
  getUsers() {
    return this.usersService.getUsers();
  }
}
