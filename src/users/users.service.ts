import { Injectable } from '@nestjs/common';
import { dbref } from 'src/firebase.config';
import { get, child } from 'firebase/database';
import { isJwtExpired } from 'jwt-check-expiration';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class UsersService {
  constructor(private jwtService: JwtService) {}

  async getUsers() {
    let success = false;
    let message = null;
    let data = null;
    let countUsers = 0;
    let countUserOnline = 0;
    let countUserOffline = 0;
    let countUserExpired = 0;

    await get(child(dbref, 'users')).then(async (snapshot) => {
      if (snapshot.exists()) {
        data = snapshot.val();
        countUsers = snapshot.size;
        for (const key in data) {
          const MyToken = data[key].token;
          if (isJwtExpired(MyToken)) {
            countUserExpired++;
          } else {
            const jwtData = await this.jwtService.verifyAsync(MyToken);
            if(jwtData.isOnline) {
              countUserOnline++;
            } else {
              countUserOffline++;
            }
          }
        }
      } else {
        console.log('No data available');
      }
    });

    return {
      success: success,
      message: message,
      countUsers: countUsers,
      countUserOnline: countUserOnline,
      countUserOffline: countUserOffline,
      countUserExpired: countUserExpired,
      data: data,
    };
  }

  async getUserInfo(MyToken) {
    let success = false;
    let message = null;
    let data = null;
    if (MyToken) {
      if (isJwtExpired(MyToken)) {
        message = 'You token expired!';
      } else {
        const jwtData = await this.jwtService.verifyAsync(MyToken);
        if (jwtData.isOnline) {
          const getUsers = await get(child(dbref, 'users')).then((snapshot) => {
            if (snapshot.exists()) {
              return snapshot.val();
            } else {
              console.log('No data available');
            }
          });

          for (const key in getUsers) {
            if (jwtData.uid == key) {
              data = getUsers[key];
              success = true;
              message = 'Get user info successfully.';
            }
          }
        } else {
          message = 'Get user info failed (You is offline)';
        }
      }

      return {
        success: success,
        message: message,
        data: data,
        token: MyToken,
      };
    } else {
      message = 'Get user info failed (Not found Token!)';
      return {
        success: success,
        message: message,
        data: data,
      };
    }
  }
}
