import { initializeApp } from 'firebase/app';
import { getDatabase, ref } from 'firebase/database';

export const firebaseConfig = {
    apiKey: "AIzaSyDkQfGorJ0oRwrt9D4J0_TIYuRUo29Cr0k",
    authDomain: "mediaplex-xr.firebaseapp.com",
    databaseURL: "https://mediaplex-xr-default-rtdb.asia-southeast1.firebasedatabase.app",
    projectId: "mediaplex-xr",
    storageBucket: "mediaplex-xr.appspot.com",
    messagingSenderId: "634444733080",
    appId: "1:634444733080:web:396b85a50df6be95e40eed",
    measurementId: "G-0VXXC170SL"
  };

export const app = initializeApp(firebaseConfig);
export const db = getDatabase();
export const dbref = ref(db);